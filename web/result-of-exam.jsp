
<%@page import="myPackage.classes.Exams"%>
<%@page import="java.util.ArrayList"%>
<jsp:useBean id="pDAO" class="myPackage.DatabaseClass" scope="page"/>

<!-- SIDEBAR -->
			<div class="sidebar" style="background-image: url(sidebar-1.jpg)">
				<div class="sidebar-background" >
					<h2 class="logo-text">
						Online Examination System
					</h2>

					<div class="left-menu">
						<a  href="adm-page.jsp?pgprt=0"><h2>Profile</h2></a>
						<a href="adm-page.jsp?pgprt=2"><h2>Courses</h2></a>
                                                <a href="adm-page.jsp?pgprt=3"><h2>Questions</h2></a>
                                                <a class="active" href="adm-page.jsp?pgprt=5"><h2>Result Of Exam</h2></a>						
                                                <a  href="adm-page.jsp?pgprt=1"><h2>Accounts</h2></a>
					</div>
				</div>
			</div>
            <!-- CONTENT AREA -->
		<% if(request.getParameter("coursename")==null){%>	
            <div class="content-area" >
                            <div class="panel form-style-6" style="min-width: 300px;max-width: 390px;float: left">
            <form action="adm-page.jsp">
                <div class="title">Show Result For </div>
                <br><br>
                <label>Select Course</label>
                <input type="hidden" name="pgprt" value="5">
                <select name="coursename" class="text">
        <% 
            ArrayList list1=pDAO.getAllCourses();
            
            for(int i=0;i<list1.size();i=i+2){
        %>
        <option value="<%=list1.get(i)%>"><%=list1.get(i)%></option>
            <%
            }
            %>
            </select>
            <input type="submit" value="Show" class="form-button">
            </form>
                            </div>
            
            <% } if(request.getParameter("coursename")!=null){%>
            
            <div class="content-area">
                            <div class="inner" style="margin-top: 50px">
                                <div class="title" style="margin-top: -30px">Result Of Students</div>
       
                                <br>
                                <br>
                                <br/>
                                
           
           
                       <table id="one-column-emphasis" >
    <colgroup>
    	<col class="oce-first" />
    </colgroup>
    <thead>
    	<tr>
        	<th scope="col">Name</th>
                
            <th scope="col">Course Name</th>
            <th scope="col">Total Marks</th>
            <th scope="col">Obtained</th>
            <th scope="col">Status</th>
            <th scope="col">Date Of Taken</th>
        </tr>
    </thead>
    <tbody>
        <% String CN=request.getParameter("coursename");
            ArrayList list=pDAO.getResultsForParticularCourse(CN);
              //ArrayList list=pDAO.getAllUsers();
              Exams exam;
              for(int i=0;i<list.size();i++){
                  exam=(Exams)list.get(i);
               %>
   
    	<tr>
        	<td><%=exam.getStdId() %></td>
            <td><%=exam.getcName() %></td>
            <td><%=exam.gettMarks() %></td>
            <td><%=exam.getObtMarks() %></td>
            <td><%=exam.getStatus() %></td>
            <td><%=exam.getDate() %></td>
        </tr>
         
               
               
               <%
                  } %>
               
                </tbody>
</table>
           
    
                            </div>
                        </div>
                  
                  <%} %>